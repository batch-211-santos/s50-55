

// My s53 Activity Solution

/*

// import Banner from '../components/Banner';
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Error (){
	return (
		<Row>
			<Col>
			<h1>404 - Page Not Found!</h1>
			<p>The Page you are looking for cannot be found!</p>
			<Button variant="primary" as={Link} to="/">Back to Home</Button>
			</Col>
		</Row>
	)

}

*/



//  Provided Solution

import Banner from '../components/Banner.js';

export default function Error (){

	const data = {
		title: "404 - Not found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return (
		// "prop" name is decided by the developer		
		<Banner bannerProp = {data}/>
	)

}


 