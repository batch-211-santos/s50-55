
// import {useState, useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function CourseCard({courseProp}){
    // console.log(courseProp)
    
    // useState hook:
    // A hook in React is a kind of tool. The useState hook allows creation and manipulation of states
    // States are a way for React to keep a track of any value and associate it with a component
    
    // state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed
    // state setter: State setters are the ONLY way to change a state's value. By convention, they are named after the state.
    // default state: The state's initial value on "page load"


    // syntax:
    // const [state, setState] = useState(defaultState)


    // array destructuring to get the state and the setter
    // const [count, setCount] = useState(0)
    // sample -> const [name, setName] = useState(0)

    let {name, description, price, _id} = courseProp;


    /*
        Activity:

        Create a state hook that will represent the number of available seats for each course

        It should default to 10, and decrement by 1 each time a student enrolls

        Add a condition that will show an alert that no more seats are available if the seats state is 0.

    */


    // const [seats, setSeats] = useState(10)

    // refactor the enroll function instead use "useEffect" hooks
    // function enroll (){
    //     if(count !== 10 ){
    //        setCount(count + 1);
    //        setSeats(seats - 1)
    //     }
    // }

    // Apply the use effect hook
    // useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)


    // Syntax:
            // useEffect(function,[dependencies])

    // useEffect(()=>{
    //     if (seats === 0){
    //         alert("No more seats available!")
    //     }
    // },[count, seats])


	return (
		<Card className="p-3 mb-3"> 
      	    <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price} PHP</Card.Text>
                <Card.Text>Enrollees: </Card.Text>
                <Card.Text>Available seats: </Card.Text>
                <Button as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
	)
}


// *component 

// the real default value is undefined
// when a component mounts, this is the only time the default value declared is used